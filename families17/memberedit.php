<!DOCTYPE html>
<html>
    <head>
        <title>Edit Member</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
//     for user pleasant to tell him as he typing not after pressing submit
    $(document).ready(function(){
    alert("ajax is ok");
    $('input[name=submit]').click(function(){
            var familyId = $('input[name=headerId]').val();
            var memberName = $('input[name=name]').val();
            $('#isTaken').load('"nametaken.php?familyId="+familyId+"&name="+memberName');
           alert($('#isTaken').val());
            
       });
    });
    
//    $(document).ready(function(){
//        alert("ajax is ok");
//    $('form').submit(function(e) { 
//
//     // this code prevents form from actually being submitted
//     e.preventDefault();
//     e.returnValue = false;
//     var $form = $(this);
//      var familyId = $('input[name=headerId]').val();
//      var memberName = $('input[name=name]').val();
//
////        // this is the important part. you want to submit
//        // the form but only after the ajax call is completed
//         $.ajax({ 
//             type: 'post',
//             url: "nametaken.php?familyId="+familyId+"&name="+memberName, 
//             context: $form, // context will be "this" in your handlers
//             success: function() { // your success handler
//               
//            
//             },
//             error: function() { // your error handler
//                $('#isTaken').load(url);  
//             },
//             complete: function() { 
//                // make sure that you are no longer handling the submit event; clear handler
//                this.off('submit');
//                // actually submit the form
//                this.submit();
//             }
//         });
////     }
//});
//   });     
//
//    
</script>
    </head>
<body>
    <div  id="centerContent">
        <div class="allignRight"><span id="isTaken"></span><a href="logout.php">Logout</a></div>
        <h1 class="center">Add new member</h1>

        
        
<?php
require_once 'db.php';

// allow access if user is logged in
if(!isset($_SESSION['user'] ))
{
    echo '<p>Access denied: you must be <a href="login.php">logged in</a> to access this page</p>';
    exit;
}

function getForm($headIdVal = "", $memberNameVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Head Id <input type="text" name="headId" value="$headIdVal"><br><br>
    Name <input type="text" name="memberName" value="$memberNameVal"></input><br><br>
   <div class="center"><input type="submit" name="submit" value="Edit member"></div>
        
</form>
      
ENDMARKER;
return $form;
}
  // get member id from url
 $memberId = isset($_GET['id']) ? $_GET['id'] : -1;
// are we receiving form submission?
if (isset($_POST['headId'])) {
    $headId = $_POST['headId'];
    $memberName = $_POST['memberName'];
    $errorList = array();
    //validation
    if ($headId < 1) {
        array_push($errorList, "head Id must be positive number");
    }
    if (strlen($memberName) < 1 || strlen($memberName) > 50) {
        array_push($errorList, "Body must be 1-50 characters long");
    }
    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
   // $body = stripUnwantedTagsAndAttrs($body);
    // strip tags except allowable tags effect
   
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($headId, $memberName);
    } else {
        // STATE 3: Successful submission
       
         // user now is id,name
        
        $authorId = $_SESSION['user']['id']; // ID of currently logged in user
        $result = mysqli_query($link, sprintf("UPDATE members SET headId='%s', name='%s' WHERE id='%s'",
            mysqli_real_escape_string($link, $headId),
            mysqli_real_escape_string($link, $memberName),
            mysqli_real_escape_string($link, $memberId)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Member is modified</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
    }
} else {
    // STATE 1: First show
   $result = mysqli_query($link, sprintf("SELECT headId, name FROM members WHERE id='%s'", mysqli_real_escape_string($link, $memberId)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $member = mysqli_fetch_assoc($result);
                if ($member) {
                    echo "<div class=article>\n";                
                    echo getForm($member['headId'],$member['name']);               
                    echo "</div>\n\n";
                } else { // 404 - not found
                    http_response_code(404);
                    echo "<p>404 - member not found <a href=index.php>click to continue</a></p>";
                }
             }
            
            ?>
            <p>To get back to index<a href="index.php">click here</a></p>
        </div>
    </body>
</html>


    


