<!DOCTYPE html>
<html>
<head>
<title>Register</title>
<link rel="stylesheet" href="styles.css">

</head>
<body>

    <div id="centerContent">
        <h1 class="center">New family registration</h1>
<?php
require_once 'db.php';
function getForm($nameVal = "",$familyNameVal="") {          // keep only valid data for name and email
    // set value=""  as default to clear pw for security
$form = <<< ENDMARKER
<form method="post">
    <label>Desired username <input type="text" name="username" value="$nameVal"></label><br><br>
    <label>Password <input type="password" name="pass" value=""></label><br><br>   
    <label>Family Name<input type="text" name="familyName" value="$familyNameVal" ></label><br><br>
    <label>First Name<input type="text" name="firstName" ></label><br><br>
    <label>Birth Date<input type="date" id="start" name="date" min="2019-01-01" max="2019-12-31">
</label><br><br>
    <div class="center"><input type="submit" value="Register!"></div>
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if(isset($_POST['username']))
{
    // get data from url
    $username= $_POST['username'];
    $pass= $_POST['pass'];
    $familyName= $_POST['familyName'];
    $firstName= $_POST['firstName'];
    $birthDate= $_POST['date'];
//    $timestamp = strtotime($birthDate);
//    $birthDate= date('m-d-Y',$timestamp);
    $errorList = array();
  
    // validation for username
    if ((strlen($username) < 4) 
            || (strlen($username) >20) 
            || (preg_match("/[a-z]/", $username) == FALSE ) 
            || (preg_match("/[0-9]/", $username) == FALSE ))
        {
            array_push($errorList, "username must be 4-20 characters, only lowercase, digits");
            $username = "";     // clear invalid data
        }
    else {
     //check if user is exist
//     $result = mysqli_query($link,sprintf("select * FROM users WHERE name='%s'", mysqli_real_escape_string($link,$username)));
//     // check if qurey succeeded in syntax 
//     if(!$result)
//      {
//          echo "Sql Query failed: " . mysqli_error($link);
//          exit;
//      }
//      // check if user is exist in database where username should be unique
//       $user = mysqli_fetch_assoc($result); // fetch first record as array
//       if($user)        // array returns true if it's not empty
//       {
//           array_push($errorList,"username is already exist");
//       }
        }
   
   
        // validation for pass
        if ((strlen($pass) < 6)
                || (strlen($pass) >100)
                || (preg_match("/[A-Z]/", $pass) == FALSE )
                || (preg_match("/[a-z]/", $pass) == FALSE )
                || (preg_match("/[0-9!@#$]/", $pass) == FALSE )) 
        {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit or special character in it");
            // do not need to clear it because we already set it as default ""
        }
        
    
    if(!$errorList)         // returns false if it's empty
    {
         // STATE 3: Successful submission               
         $result = mysqli_query($link, sprintf("INSERT INTO heads VALUES (NULL, '%s', '%s', '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $username),
            mysqli_real_escape_string($link, $pass),
            mysqli_real_escape_string($link, $familyName),
            mysqli_real_escape_string($link, $firstName),
            mysqli_real_escape_string($link, $birthDate)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "Welcome! $username,  you'r registered successfully";
        echo '<p><a href="login.php">Click here to login</a></p>';
        
    }
    else
    {
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class='errorMessage'>$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($username,$familyName);
    }    
    
}

else
{
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>