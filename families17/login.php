<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div  id="centerContent">
        <h1 class="center">User login</h1>
        
        <?php

require_once 'db.php';

// here-document or "here-doc"
function getForm($usernameVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Username: <input type="text" name="username" value="$usernameVal"><br><br>
    Password: <input type="password" name="password"><br><br>
   <div class="center"><input type="submit" value="Login"></div>
        
</form>
       
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['username'])) {
    $username = $_POST['username'];    
    $password = $_POST['password'];
    // bool var to set it true if pw is ok
    $loginSuccessful = false;
    // select username try to login record
    $result = mysqli_query($link, sprintf("SELECT * FROM heads WHERE username='%s'",
            mysqli_real_escape_string($link, $username)));
    // check if query syntax is ok
    if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
    }
    // fetch first record id, username, password as array in result query set
    $user = mysqli_fetch_assoc($result);
     // check first record id, username, password
    if ($user) {// true it means username is exist
        // we need to check his password
        if ($user['password'] == $password) {
            
            $loginSuccessful = true;
        }        
    }    
    
    if (!$loginSuccessful) { // pw is not correct
        // STATE 2: Failed submission
        echo "<p>Login failed<p>\n";
        echo getForm($username);
    } else {
        // STATE 3: Successful submission
        echo "<p>Login successful</p>";
        echo '<p><a href="index.php">Click to continue</a></p>';
        unset($user['password']); // remove password from array for security reasons
        // user now is id,name
        $_SESSION['user'] = $user;      // create new array named user and store it in var $_SESSION
        
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
        <div class="allignRight">
            <a href="register.php">No account? Register here</a>
        </div>
    </div>
</body>
</html>



