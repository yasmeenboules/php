<!DOCTYPE html>
<html>
    <head>
        <title>Add Member</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
//     for user pleasant to tell him as he typing not after pressing submit
    $(document).ready(function(){
   // alert("ajax is ok");
    $('input[name=memberName]').keyup(function(){
            var familyId = $('input[name=headId]').val();
             var memberName = $('input[name=memberName]').val();
            $('#isTaken').load("nametaken.php?familyId="+familyId+"&name="+memberName);
           
            
       });
    });
    
//   
</script>
    </head>
<body>
    <div  id="centerContent">
        <div class="allignRight"><a href="logout.php">Logout</a></div>
        <h1 class="center">Add new member</h1>

        
        
<?php
require_once 'db.php';

// allow access if user is logged in
if(!isset($_SESSION['user'] ))
{
    echo '<p>Access denied: you must be <a href="login.php">logged in</a> to access this page</p>';
    exit;
}

function getForm($headIdVal = "", $memberNameVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Head Id <input type="text" name="headId" value="$headIdVal"><br><br>
    Name <input type="text" name="memberName" value="$memberNameVal"></input><span class='errorMessage' id="isTaken"></span><br><br>
   <div class="center"><input type="submit" name="submit" value="Add member"></div>
        
</form>
      
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['headId'])) {
    $headId = $_POST['headId'];
    $memberName = $_POST['memberName'];
    $errorList = array();
    //validation
    if ($headId < 1) {
        array_push($errorList, "head Id must be positive number");
    }
    if (strlen($memberName) < 1 || strlen($memberName) > 50) {
        array_push($errorList, "Body must be 1-50 characters long");
    }
    // check if headId for specific member is exist
    $result = mysqli_query($link, sprintf("SELECT * FROM members WHERE headId='%s'",
        mysqli_real_escape_string($link, $headId)
        ));
    // check if query succeed or no for syntax err
    if (!$result) {

        echo "SQL Query failed: " . mysqli_error($link);
        exit;
    }
    // check first record as array
    // array return false if it's empty
    $member = mysqli_fetch_assoc($result);
    if ($member['name'] == $memberName) {
        array_push($errorList, "member is already exist in that family, try a different one");
    }

   
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($headId, $memberName);
    } else {
        // STATE 3: Successful submission
       
         // user now is id,name
        
        $authorId = $_SESSION['user']['id']; // ID of currently logged in user
        $result = mysqli_query($link, sprintf("INSERT INTO members VALUES (NULL, '%s', '%s')",
            mysqli_real_escape_string($link, $headId),
            mysqli_real_escape_string($link, $memberName)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Member is added successfully</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>


    
