


<?php
// check if parameters are set in the url
if(!isset($_GET['min']) || !isset($_GET['max']))
{
    die("Error: min and max must be provided");
}
// we do not need to store values in variables but for easy use 
$min = $_GET['min'];
$max = $_GET['max'];
// validation if values are numeric
if(!is_numeric($min) || !is_numeric($max))
{
    die("Error: min and max must be non-empty integer values");
}
// validation if max>min
if($min>$max)
{
    die("Error: minimum must not be greater than maximum");
}
// generate random number bet min , max
$num = rand($min,$max);
echo $num;
