<!DOCTYPE html>
<html>
    <head>
        <title>Add Article</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div  id="centerContent">
        <div class="allignRight"><span id="isTaken"></span><a href="logout.php">Logout</a></div>
        <h1 class="center">Create new article</h1>

        
        
<?php
require_once 'db.php';

// allow access if user is logged in
if(!isset($_SESSION['user'] ))
{
    echo '<p>Access denied: you must be <a href="login.php">logged in</a> to access this page</p>';
    exit;
}

function getForm($titleVAl = "", $bodyVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Title <input type="text" name="title" value="$titleVal"><br><br>
    Content <textarea cols="60" rows="10" name="body">$bodyVal</textarea><br><br>
   <div class="center"><input type="submit" value="Create"></div>
        
</form>
      
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['title'])) {
    $title = $_POST['title'];
    $body = $_POST['body'];
    $errorList = array();
    //
    if (strlen($title) < 5 || strlen($title) > 200) {
        array_push($errorList, "Title must be 5-200 characters long");
    }
    if (strlen($body) < 5 || strlen($body) > 65000) {
        array_push($errorList, "Body must be 5-65000 characters long");
    }
    //
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($title, $body);
    } else {
        // STATE 3: Successful submission
        echo "<p>Article added successfully</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
        //
        $authorId = $_SESSION['user']['id']; // ID of currently logged in user
        $result = mysqli_query($link, sprintf("INSERT INTO articles VALUES (NULL, '%s', NULL, '%s', '%s')",
            mysqli_real_escape_string($link, $authorId),
            mysqli_real_escape_string($link, $title),
            mysqli_real_escape_string($link, $body)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>
