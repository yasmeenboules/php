<!DOCTYPE html>
<html>
<head>
<title>Register</title>
<link rel="stylesheet" href="styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('input[name=name]').keyup(function(){
            var username = $('input[name=name]').val();
            $('#isTaken').load("istaken.php?username="+username);           
        });
    });
</script>
</head>
<body>

    <div id="centerContent">

<?php
require_once 'db.php';
function getForm($nameVal = "",$emailVal="") {
$form = <<< ENDMARKER
<form method=POST>
    <b>Username:</b><input type="text" name="name" value="$nameVal"><span class='errorMessage' id="isTaken"></span><br><br>
    <b>Email:</b><input type="text" name="email" value=""><br><br>
    <b>Password:</b><input type="password" name="pass1" value=""><br><br>
    <b>Confirm Password:</b><input type="password" name="pass2" value=""><br><br>
    <input type="submit">
</form>
ENDMARKER;
return $form;
}

if(isset($_POST['name']))
{
 
 
    // get data from url
    $name = $_POST['name'];
    $email = $_POST['email'];
    $pass1= $_POST['pass1'];
    $pass2= $_POST['pass2'];
    $errorList = array();
    $namePattern = '/^[a-zA-Z0-9_]{6,20}$/';
   
    
    
    // validation
    if(!preg_match($namePattern, $name))
    {
        array_push($errorList, "name must be 6-20 characters, only uppercase/lowercase, digits, underscore");
        $name = "";
    }
 else {
     $result = mysqli_query($link,sprintf("select * FROM users WHERE username='%s'", mysqli_real_escape_string($link,$name)));
      if(!$result)
      {
          echo "Sql Query failed: " . mysqli_error($link);
          exit;
      }
       $user = mysqli_fetch_assoc($result);
       if($user)
       {
           array_push($errorList,"username is already exist");
       }
    }
    if(!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        array_push($errorList, "invalid email address");
        $email = "";
    }
   
    if($pass1 != $pass2)
    {
        array_push($errorList, "confirm your passward by typing the same password");
        $pass2 = "";
    }
    else
    {
        if ((strlen($pass1) < 6)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) 
        {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }
    if(!$errorList)
    {
        echo "Welcome! $name, your are registered successfully";
         $result = mysqli_query($link, sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $name),
            mysqli_real_escape_string($link, $email),
            mysqli_real_escape_string($link, $pass1)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        
    }
    else
    {
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class='errorMessage'>$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name,$email);
    }    
    
}

else
{
    // get data from registration form
    echo getForm();
}

?>
    </div>
</body>
</html>