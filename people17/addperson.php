<!DOCTYPE html>
<html>
<head>
<title>Register</title>
<link rel="stylesheet" href="styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('input[name=name]').keyup(function(){
            var name = $('input[name=name]').val();
            $('#isTaken').load("istaken.php?name="+name);           
        });
    });
</script>
</head>
<body>



<div id="centerContent">

<?php

require_once 'db.php';
function getForm($nameVal = "",$gpaVal="", $isGraduateVal=false,$genderVal="male") {
  // TODO: handle checkbox and radio buttons values coming in
$isGradChecked = $isGraduateVal ? 'checked' : '';
$rbMaleChecked = $rbFemaleChecked = $rbOtherChecked = "";
switch ($genderVal) {
    case "male": $rbMaleChecked = 'checked'; break;
    case "female": $rbFemaleChecked = 'checked'; break;
    case "other": $rbOtherChecked = 'checked'; break;
    default: // ???
    }
    
    $form = <<< ENDMARKER
<form method="post">
    Name <input type="text" name="name" value="$nameVal"><span class='errorMessage' id="isTaken"></span><br><br>
    GPA <input type="number" step="0.01" name="gpa" value="$gpaVal"><br><br>
     Is graduate? <input type="checkbox" name="isGraduate" value="true" $isGradChecked><br>
    Gender: <input type="radio" name="gender" value="male" $rbMaleChecked>Male</input>
    <input type="radio" name="gender" value="female" $rbFemaleChecked>Female</input>
    <input type="radio" name="gender" value="other" $rbOtherChecked>Other</input><br><br><br>
    <div class="center"><input type="submit" value="Add person!"></div>
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if(isset($_POST['name']))
{
    // get data from url
    $name= $_POST['name'];
    $gpa = $_POST['gpa'];
    $isGraduate= isset($_POST['isGraduate']);
    $gender= $_POST['gender'];
    $errorList = array();
    
     // validation
    if ((strlen($name) < 1) 
        || (strlen($name) >50))
    {
        array_push($errorList, "name must be 1-50 characters");
    }
   
   else 
    {
       //  check person is not exist
        $result = mysqli_query($link,sprintf("select * FROM people WHERE name='%s'", mysqli_real_escape_string($link,$name)));
        if(!$result)
        {
            echo "Sql Query failed: " . mysqli_error($link);
            exit;
        }
         $person = mysqli_fetch_assoc($result);
         if($person)
         {
             array_push($errorList,"person is already exist");
         }
    }
    //validation
    if (!is_numeric($gpa) || ($gpa < 0)|| ($gpa >4.3))
   {
       array_push($errorList, "GPA must be 0-4.3 characters");
   }
   
    if(!$errorList)
    {
         // STATE 3: Successful submission
       
        
         $result = mysqli_query($link, sprintf("INSERT INTO people VALUES (NULL, '%s', '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $name),
            mysqli_real_escape_string($link, $gpa),
            mysqli_real_escape_string($link, $isGraduate ? 'true' : 'false'),
            mysqli_real_escape_string($link, $gender)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "Welcome! $name,  person is added successfully";
        echo '<p><a href="list.php">Click here to view list of people</a></p>';
        
    }
    else
    {
         // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class='errorMessage'>$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name,$gpa,$isGraduate,$gender);
    }
}
else
{
    // STATE 1: First show
    echo getForm();
}
       
   
    

?>
</div>