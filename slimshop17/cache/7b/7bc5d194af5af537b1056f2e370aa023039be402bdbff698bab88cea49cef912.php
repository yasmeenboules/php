<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/products_list.html.twig */
class __TwigTemplate_60c2d8a99efef82cb402210f52cca3563d2a26559c08733e32ec350de2a380d1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "admin/products_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        // line 3
        echo "<p><a href=\"/admin/products/add\">Add product</a></p>
<table border=\"1\">
    <tr><th>#</th><th>Name</th><th>Description</th><th>Price</th><th>image</th><th>actions</th>
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 7
            echo "               <tr>
                  <td>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", []), "html", null, true);
            echo "</td> 
                  <td>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "name", []), "html", null, true);
            echo "</td> 
                  <td>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "description", []), "html", null, true);
            echo "</td> 
                  <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "price", []), "html", null, true);
            echo "</td>
                  <td><img src=\"/";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "imagePath", []), "html", null, true);
            echo "\" alt=\"\" width=\"100\"></td> 
                  <td>
                      <button onclick=\"window.location='/admin/products/edit/";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", []), "html", null, true);
            echo "'\">Edit</button>
                      <button onclick=\"window.location='/admin/products/delete/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", []), "html", null, true);
            echo "'\">Delete</button>
                  </td> 
               </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "</table>
";
    }

    public function getTemplateName()
    {
        return "admin/products_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 19,  79 => 15,  75 => 14,  70 => 12,  66 => 11,  62 => 10,  58 => 9,  54 => 8,  51 => 7,  47 => 6,  42 => 3,  39 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block content  %}
<p><a href=\"/admin/products/add\">Add product</a></p>
<table border=\"1\">
    <tr><th>#</th><th>Name</th><th>Description</th><th>Price</th><th>image</th><th>actions</th>
        {% for p in list %}
               <tr>
                  <td>{{p.id}}</td> 
                  <td>{{p.name}}</td> 
                  <td>{{p.description}}</td> 
                  <td>{{p.price}}</td>
                  <td><img src=\"/{{p.imagePath}}\" alt=\"\" width=\"100\"></td> 
                  <td>
                      <button onclick=\"window.location='/admin/products/edit/{{p.id}}'\">Edit</button>
                      <button onclick=\"window.location='/admin/products/delete/{{p.id}}'\">Delete</button>
                  </td> 
               </tr>
            {% endfor %}
</table>
{% endblock content  %}
", "admin/products_list.html.twig", "C:\\xampp\\htdocs\\ipd17\\slimshop17\\templates\\admin\\products_list.html.twig");
    }
}
