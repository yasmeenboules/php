<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/products_add.html.twig */
class __TwigTemplate_145a790a7187f245cf06bb6560c56a71fb1dd6657f4bde7beabcce8eb202d8b6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "admin/products_add.html.twig", 4);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        // line 7
        echo "    <div id=\"centerContent\">
    ";
        // line 8
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 9
            echo "        <ul>
            ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "                <li class=\"errorMessage\">";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "        </ul>
    ";
        }
        // line 15
        echo "
    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", []), "html", null, true);
        echo "\"><br>
        Description: <textarea  name=\"description\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "description", []), "html", null, true);
        echo "\"></textarea><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value=";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "price", []), "html", null, true);
        echo "><br>
        <p>todo: image upload</p>
        <input type=\"submit\" value=\"Add product\">
    </form>
        </div>
";
    }

    public function getTemplateName()
    {
        return "admin/products_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 19,  75 => 18,  71 => 17,  67 => 15,  63 => 13,  54 => 11,  50 => 10,  47 => 9,  45 => 8,  42 => 7,  39 => 6,  29 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("


{% extends \"master.html.twig\" %}

{% block content  %}
    <div id=\"centerContent\">
    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li class=\"errorMessage\">{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
        Description: <textarea  name=\"description\" value=\"{{v.description}}\"></textarea><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value={{v.price}}><br>
        <p>todo: image upload</p>
        <input type=\"submit\" value=\"Add product\">
    </form>
        </div>
{% endblock content %}
", "admin/products_add.html.twig", "C:\\xampp\\htdocs\\ipd17\\slimshop17\\templates\\admin\\products_add.html.twig");
    }
}
