<!DOCTYPE html>
<html>
    <head>
        <title>Add Owner</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div  id="centerContent">

        
        <h1 class="center">Add New Owner</h1>

        
        
<?php
require_once 'db.php';

function getForm($nameVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Name <input type="text" name="name" value="$nameVal"><br><br>
   <div class="center"><input type="submit" value="Add Owner"></div>        
</form>
      
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['name'])) {
    $name=$_POST['name'];
    $errorList = array();
    //validation
    if (strlen($name) < 1 || strlen($name) > 50) {
        array_push($errorList, "name must be 1-50 characters long");
        $name="";
    }
   
    
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name);
    } else {
        // STATE 3: Successful submission
      
        $result = mysqli_query($link, sprintf("INSERT INTO owners VALUES (NULL, '%s')",
            mysqli_real_escape_string($link, $name)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Owner is added successfully</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>

<?php
