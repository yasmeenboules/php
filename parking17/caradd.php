<!DOCTYPE html>
<html>
    <head>
        <title>Add Car</title>
        
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div  id="centerContent">
       

        
        
<?php

require_once 'db.php';
function getForm($ownerIdVal="",$modelVal = "", $platesVal = "",$engSizeVal="",$fuelTypeVal="gasoline") {
    global $link;
        $options = '';
    $result=mysqli_query($link,"select * from owners");
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        } 
        $SelectedOwnerId="";
    while($row = mysqli_fetch_assoc($result)) {
        if($row['id']==$ownerIdVal)
        {
            $SelectedOwnerId = "selected";
        }else
        {
            $SelectedOwnerId="";
        }
        $options .="<option $SelectedOwnerId>" . $row['id'].":".$row['name'] . "</option>";
    }
    $rbGasolineChecked = $rbDieselChecked = $rbHybridChecked = $rbElectricChecked =$rbOtherChecked = "";
    switch ($fuelTypeVal) {
        case "gasoline": $rbGasolineChecked = 'checked'; break;
        case "diesel": $rbDieselChecked = 'checked'; break;
        case "hybrid": $rbHybridChecked = 'checked'; break;
        case "electric": $rbElectricChecked = 'checked'; break;
        case "other": $rbOtherChecked = 'checked'; break;
        default: // ???
        }
    
$form = <<< ENDMARKER
<form method="post">
        
    Owner Id<select name='ownerId' >" . $options . "</select><br><br><br>
    Make Model<input type="text" name="model" value="$modelVal"><br><br>
    Plates <input type="text" name="plates" value="$platesVal"><br><br>
    Engine Size <input type="number" step="0.01" name="engSize" value="$engSizeVal"><br><br><br>
    Fuel Type 
        <input type="radio" name="fuel" value="gasoline" $rbGasolineChecked> gasoline
        <input type="radio" name="fuel" value="diesel" $rbDieselChecked> diesel
        <input type="radio" name="fuel" value="hybrid" $rbHybridChecked> hybrid
        <input type="radio" name="fuel" value="electric"$rbElectricChecked> electric
        <input type="radio" name="fuel" value="other" $rbOtherChecked> other<br><br><br>
   <div class="center"><input type="submit" value="Add Car"></div>
        
</form>
      
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['ownerId']) && $_POST['ownerId'] !="" ) {
    $ownerId=$_POST['ownerId'];
    $model = $_POST['model'];
    $plates = $_POST['plates'];
    $engSize = $_POST['engSize'];
    $fuel = $_POST['fuel'];
    $errorList = array();
    //validation
    if (strlen($model) < 1 || strlen($model) > 50) {
        array_push($errorList, "Model must be 1-50 characters long");
        $model="";
    }
    if ((strlen($plates) < 1 || strlen($plates) > 10 
            || (preg_match("/[a-z]/", $plates) == FALSE ) 
        || (preg_match("/[0-9]/", $plates) == FALSE ))) {
        array_push($errorList, "Plates must be 1-10 characters long composed of letters (uppercase or lowercase) and digits");
        $plates="";
    }
    if($engSize!="")
    {
       if(!is_numeric($engSize) || $engSize <0 || $engSize>99.99)
        {
            array_push($errorList,"engine size should be a numerical value between 0-99.99");
            $engSize="";
        } 
    }
   
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($ownerId,$model, $plates,$engSize,$fuel);
    } else {
        // STATE 3: Successful submission
       
  
        
        // convert plates to uppercase
        $plates = strtoupper($plates);
        $result = mysqli_query($link, sprintf("INSERT INTO cars VALUES (NULL,'%s', '%s', '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $ownerId),
            mysqli_real_escape_string($link, $model),
            mysqli_real_escape_string($link, $plates),
            mysqli_real_escape_string($link, $engSize),
            mysqli_real_escape_string($link, $fuel)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Car is added successfully</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>


