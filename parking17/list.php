<!DOCTYPE html>
<html>
<head>
<title>List of people</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="styles.css">

</head>
<body>
<div id="centerContent">
<table class="table table-striped center"> 
<thead>
    <tr class="row">
        <th class="col-sm-2">Owner</th>
        <th class="col-sm-2">Make Model</th>
        <th class="col-sm-2">Plates</th>
        <th class="col-sm-2">Engine Size</th>
        <th class="col-sm-2">Fuel Type</th>
        <th class="col-sm-2">Modify Column</th>
    </tr>
</thead>

<?php
require_once 'db.php';
$result = mysqli_query($link,"select cars.id, name, makeModel,plates,engineSizeL,fuelType FROM cars inner join owners on cars.ownerId =owners.id");  // result is list of records
if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $member = mysqli_fetch_assoc($result);
                if ($member) {
                    echo "<div class=article>\n";                
                    echo getForm($member['headId'],$member['name']);               
                    echo "</div>\n\n";
                } else { // 404 - not found
                    http_response_code(404);
                    echo "<p>404 - member not found <a href=index.php>click to continue</a></p>";
                }
// mysqli_fetch_assoc($result) function to fetch records one by one
// return false if it points to empty
while($row = mysqli_fetch_assoc($result)){   
//row is temp record in while Creates a loop to loop through results

echo "<tr class='row'>"
        . "<td class='col-sm-2'>" . $row['name'] . "</td>"
        . "<td class='col-sm-2'>" . $row['makeModel']. "</td>"
        . "<td class='col-sm-2'>" . $row['plates']. "</td>"
        . "<td class='col-sm-2'>" . $row['engineSizeL']. "L</td>"
        . "<td class='col-sm-2'>" . $row['fuelType']."</td>"
        . "<td class='col-sm-2'><a href=caraddedit.php?id=".$row['id'].">Edit</a></td>"
    . "</tr>";  //$row['index'] the index here is a field name

}
echo '</table>'; 
echo '<p><a href="index.php">Click here to continue</a></p>';
echo '</div>';  

//Close the table in HTML
?>

 
    
    
    
    
    
    
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>