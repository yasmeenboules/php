<!DOCTYPE html>
<html lang="en">
<head>
<title>Shout</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>

    <div id="centerContent">
        <h1 class="center">New Shout</h1>
<?php
require_once 'db.php';
function getForm($nameVal = "") {          // keep only valid data for name and email
    // set value=""  as default to clear pw for security
$form = <<< ENDMARKER
<form method="post">
    <label>Name <input type="text" name="name" value="$nameVal"></label><br><br>
    <label>Message <input type="text" name="msg" value=""></label><br><br>
    <div class="center"><input type="submit" value="Shout"></div>
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if(isset($_POST['name']))
{
    // get data from url
    $username= $_POST['name'];
    $message = $_POST['msg'];
    $namePattern = '/^[a-zA-Z0-9_ ]{2,20}$/';
   
    
    $errorList = array();
  
    // validation for username
    if(!preg_match($namePattern, $username))
    {
        array_push($errorList, "name must be 2-20 characters, only uppercase/lowercase, digits, underscore, spaces");
        $username = "";
    }
      
    // validation for message
   
    if ((strlen($message) < 1) || (strlen($message) >100)) 
    {
        array_push($errorList, "Message must be 1-100 characters long ");
               
    }
    
    if(!$errorList)         // returns false if it's empty
    {
         // STATE 3: Successful submission
        if(!isset($_SESSION['count']))
        {
            $_SESSION['count']=0;
        }
        $_SESSION['count']++;
         $result = mysqli_query($link, sprintf("INSERT INTO shouts VALUES (NULL,'%s','%s',NULL)",
            mysqli_real_escape_string($link, $username),
            mysqli_real_escape_string($link, $message)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "Thank you! $username,  your message successfully submitted";
        echo getForm();
    }
    else
    {
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class='errorMessage'>$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($username);
    } 
    // On both successful or failed submission
   
    $result = mysqli_query($link,"SELECT * FROM shouts ORDER BY tsPosted DESC");
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        $num=0;
        while($row = mysqli_fetch_assoc($result))
        {   
            $num++;
            if($num==11)
            {
                break;
            }
            echo "on " . $row['tsPosted']." ". $row['name']." ". "shouted: " . $row['message'];
            echo "<br>";
        }
        echo "<br><br>";
        echo "You've shouted ".$_SESSION['count']." times"; 
        echo "<br>";
}

else
{
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>