<!DOCTYPE html>
<html>
    <head>
        <title>Edit Article</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
        <script>tinymce.init({selector:'textarea'});</script>
    </head>
    <body>
        <div id="centerContent">


<?php
function getForm($titleVal = "", $bodyVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Title <input type="text" name="title" value="$titleVal"><br><br>
    Content <textarea cols="60" rows="10" name="body">$bodyVal</textarea><br><br>
   <div class="center"><input type="submit" value="Update"></div>
        
</form>
      
ENDMARKER;
return $form;
}
            require_once 'db.php'; 
            // to allow access this page for only who are login in
            if (isset($_SESSION['user'])) {
                $user = $_SESSION['user'];
                echo "<p>You are logged in as user " .$user['name'];                                            
                echo "You may <a href=articleedit.php>edit an article</a>";
                        
            } else {
                echo "<p>You are NOT logged in. You may <a href=login.php>log in</a>"
                ."or <a href=register.php>register</a></p>";
                echo "<a href=index.php>back to homepage</a>";
            }
            // get article id from url
            $articleId = isset($_GET['id']) ? $_GET['id'] : -1;
            
            // are we receiving form submission?
        if (isset($_POST['title'])) 
        {
            $title = $_POST['title'];
            $body = $_POST['body'];
            $errorList = array();
            //
            if (strlen($title) < 5 || strlen($title) > 200) {
                array_push($errorList, "Title must be 5-200 characters long");
            }
            if (strlen($body) < 5 || strlen($body) > 65000) {
                array_push($errorList, "Body must be 5-65000 characters long");
            }
            // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
            // $body = stripUnwantedTagsAndAttrs($body);
            $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4>");
            if ($errorList) { // array not empty -> errors present
                // STATE 2: Failed submission
                echo "<p>There were problems with your submission:</p>\n<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li class=\"errorMessage\">$error</li>\n";
                }
                echo "</ul>\n";
                echo getForm($title, $body);
            } 
            else
            {
                // STATE 3: Successful submission
                
                //
                $authorId = $_SESSION['user']['id']; // ID of currently logged in user
                $result = mysqli_query($link, sprintf("UPDATE articles SET title='%s', body='%s' WHERE id='%s'",
                    mysqli_real_escape_string($link, $title),
                    mysqli_real_escape_string($link,$body ),
                    mysqli_real_escape_string($link,$articleId )));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                echo "<p>Article is modified by ". $_SESSION['user']['name']."</p>";
                echo '<p><a href="index.php">Click here to continue</a></p>';
            }
        } 
        else {            
                
                $result = mysqli_query($link, sprintf("SELECT articles.id, name, posted, "
                                . "title, body FROM articles, users WHERE articles.authorId = users.id "
                                . "AND articles.id='%s'", mysqli_real_escape_string($link, $articleId)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $article = mysqli_fetch_assoc($result);
                if ($article) {
                    echo "<div class=article>\n";                
                    echo getForm($article['title'],$article['body']);               
                    echo "</div>\n\n";
                } else { // 404 - not found
                    http_response_code(404);
                    echo "<p>404 - Article not found <a href=index.php>click to continue</a></p>";
                }
             }
            
            ?>
            <p>To get back to index<a href="register.php">click here</a></p>
        </div>
    </body>
</html>
