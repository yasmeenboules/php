<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div id="centerContent">

<?php

 
       
        require_once 'db.php';                     
            if (isset($_SESSION['user'])) {
                $user = $_SESSION['user'];       // notice that user is id,name no pw
                echo "<p>You are logged in as user " .$user['name'].
                        ". You may <a href=articleadd.php>post a new article</a>"
                        ." or <a href=logout.php>logout</a></p>";
                echo "You may also <a href=articleedit.php>edit an existing article</a>";
                        
            } else {
                echo "<p>You are NOT logged in. You may <a href=login.php>log in</a>"
                ."or <a href=register.php>register</a></p>";
                echo "<a href=index.php>back to homepage</a>";
            }
          //
            $result = mysqli_query($link,"SELECT articles.id, name, posted, "
                    . "title, body FROM articles, users WHERE articles.authorId = users.id");
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            
            while ($article = mysqli_fetch_assoc($result)) {
                echo "<div class=article>\n";
                echo "<a href=article.php?id=".$article['id']."><h1>" . $article['title'] . "</h1></a>\n";
                echo "<h2>Posted by " . $article['name'] . " on " . $article['posted'] . "</h2>\n";
                $text = strip_tags($article['body']);
                if (strlen($text) > 50) {
                    $text = substr($text, 0, 50) . "...";
                }
                echo "<p>" . $article['body'] . "</p>";
                echo "</div>\n\n";
            }
            
        ?>
    </div>
    </body>
</html>
