<?php
require_once 'db.php';
$username = $_GET['name'];

$result = mysqli_query($link, sprintf("SELECT * FROM users WHERE name='%s'",
        mysqli_real_escape_string($link, $username)));
// check if query succeed or no for syntax err
if (!$result) {
    echo "SQL Query failed: " . mysqli_error($link);
    exit;
}
// check first record as array
// array return false if it's empty
$user = mysqli_fetch_assoc($result);
if ($user) {
    echo "Username already registered, try a different one";
}

