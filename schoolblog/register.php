<!DOCTYPE html>
<html>
<head>
<title>Register</title>
<link rel="stylesheet" href="styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    // for user pleasant to tell him as he typing not after pressing submit
    $(document).ready(function(){
        alert("hi");
        $('input[name=name]').keyup(function(){
            var username = $('input[name=name]').val();
            $('#isTaken').load("istaken.php?name="+username);           
        });
    });
</script>
</head>
<body>

    <div id="centerContent">
        <h1 class="center">New User registration</h1>
<?php
require_once 'db.php';
function getForm($nameVal = "",$emailVal="") {          // keep only valid data for name and email
    // set value=""  as default to clear pw for security
$form = <<< ENDMARKER
<form method="post">
    <label>Desired username <input type="text" name="name" value="$nameVal"></label><span class='errorMessage' id="isTaken"></span><br><br>
    <label>Your email <input type="text" name="email" value="$emailVal"></label><br><br>
    <label>Password <input type="password" name="pass1" value=""></label><br><br>   
    <label>Password (repeat) <input type="password" name="pass2" value=""></label><br><br>
    <div class="center"><input type="submit" value="Register!"></div>
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if(isset($_POST['name']))
{
    // get data from url
    $username= $_POST['name'];
    $email = $_POST['email'];
    $pass1= $_POST['pass1'];
    $pass2= $_POST['pass2'];
    $errorList = array();
  
    // validation for username
if ((strlen($username) < 4) 
        || (strlen($username) >20) 
        || (preg_match("/[a-z]/", $username) == FALSE ) 
        || (preg_match("/[0-9]/", $username) == FALSE ))
    {
        array_push($errorList, "username must be 4-20 characters, only lowercase, digits");
        $username = "";     // clear invalid data
    }
 else {
     //check if user is exist
     $result = mysqli_query($link,sprintf("select * FROM users WHERE name='%s'", mysqli_real_escape_string($link,$username)));
     // check if qurey succeeded in syntax 
     if(!$result)
      {
          echo "Sql Query failed: " . mysqli_error($link);
          exit;
      }
      // check if user is exist in database where username should be unique
       $user = mysqli_fetch_assoc($result); // fetch first record as array
       if($user)        // array returns true if it's not empty
       {
           array_push($errorList,"username is already exist");
       }
    }
    // validation for email
    if(!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        array_push($errorList, "invalid email address");
        $email = "";    // clear invalid data
    }
   // validation for pass2
    if($pass1 != $pass2)
    {
        array_push($errorList, "confirm your passward by typing the same password");
        $pass2 = "";    // clear invalid data
    }
    else
    {
        // validation for pass1
        if ((strlen($pass1) < 6)
                || (strlen($pass1) >100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9!@#$]/", $pass1) == FALSE )) 
        {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit or special character in it");
            // do not need to clear it because we already set it as default ""
        }
    }
    if(!$errorList)         // returns false if it's empty
    {
         // STATE 3: Successful submission
        echo "Welcome! $username,  your are registered successfully";
        echo '<p><a href="login.php">Click here to login</a></p>';
        
         $result = mysqli_query($link, sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $username),
            mysqli_real_escape_string($link, $email),
            mysqli_real_escape_string($link, $pass1)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        
    }
    else
    {
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class='errorMessage'>$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($username,$email);
    }    
    
}

else
{
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>